1. Create a class for video src.
2. Select the video.
3. Implements buttons for  Play, Forward, Rewind.
4. Create a class for Play/Pause.
5. if(videoplayer.paused)
6. then return videoplayer.play().
7. else return videoplayer.pause().
8. Create a class for Forward.
9. Return videoplayer.currentTime += 3. (For 3s Forward)
10. Create a class for Rewind.
11. Return videoplayer.currentTime += -5. (For 5s Backward)
12. In main class Create an object for Play,Forward,Rewind class as play,forward,rewind.
13. when play button clicked call play object.
14. when forward button clicked call forward object.
15. when rewind button clicked call backward object.



Here I am using Facade Pattern for this application. 
Beacause Video will be the main class and we can so many option around this class.
Like Play, Reload, Forward , Backward, Save currentTime etc.
