# Bongo Written Test

Below is the written test for Bongo’s Frontend Developer position. Please read through
the entire test before starting to write it.

## Problem 1:

Write a function that detects if two strings are an anagram e.g. ‘bleat’ and ‘table’ are
anagrams but ‘eat’ and ‘tar’ are not.

## Problem 2:

Explain the design pattern used in following:

```
interface Vehicle {
int set_num_of_wheels()
int set_num_of_passengers()
boolean has_gas()
}
```

- Explain how can you use the pattern to create car and plane class?
- Use a different design pattern for this solution.

## Problem 3:

Write a video player application with ‘Play’, ‘Forward’ , ‘Rewind’ functionalities. Please
write pseudocode for this program and explain the design pattern you will use to develop
these three functionalities.
