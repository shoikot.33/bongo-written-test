function create_char_map(data){
	let char_map={};
	for (let char of data){
		if(char_map.hasOwnProperty(char)){
			char_map[char]++;
		}
		else
			char_map[char]=1;
	}
	return char_map;
}

function is_anagram(dataA,dataB){
	if(dataA.length==dataB.length){
		let arrow = create_char_map(dataA);
		let bow = create_char_map(dataB);
		for(let char in arrow){
			if(arrow[char]!= bow[char]){
				return false;
			}
		}
		return true;
	}
	else
		return false;
}

//test1
var rslt = is_anagram('eat', 'tar');
console.log("Unit Test Number 1: "+rslt)

//test2
var unit_test = is_anagram('bleat', 'table');
console.log("Unit Test Number 2: "+ unit_test);