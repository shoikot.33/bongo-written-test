package com.company;


import com.company.FactoryDesignPattern.vehicleFactory;
import com.company.FactoryDesignPattern.vehiclePattern;

public class Main {

    public static void main(String[] args) {
        //**************************************** TEST 1 ************************************************
        //In Object oriented Design, Coupling refers to the degree of direct knowledge that one element has of another.
        // In Other words, how often do changes in Class Car force related changes in class Plane. In order to removing is problem we have to use interface design pattens.
        //Our Goal is to achieve loose coupling.

        //Achieve loose Coupling
        //Create different interface for each class
        Vehicle car = getCar();
        Vehicle plane = getPlane();
        //set Interface value for Car Class
        car.set_num_of_wheels();
        car.set_num_of_passengers();
        car.has_gas();
        //System.out.println(car.set_num_of_wheels());
        //set Interface value for Plane Class
        plane.set_num_of_wheels();
        plane.set_num_of_passengers();
        plane.has_gas();

//**************************************** TEST 2 ************************************************
        //Test 2
        //Factory Design Patterns
        vehicleFactory obj = new vehicleFactory();
        vehiclePattern Car = obj.getInstance(4);
        vehiclePattern Plane = obj.getInstance(10);
        Car.print();
        Plane.print();


    }
    public  static  Vehicle getCar(){
        return new Car();
    }
    public static Vehicle getPlane(){
        return new Plane();
    }
}
