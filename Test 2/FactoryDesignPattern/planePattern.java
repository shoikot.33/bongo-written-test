package com.company.FactoryDesignPattern;

public class planePattern implements vehiclePattern {
    @Override
    public int set_num_of_wheels() {
        return 18;
    }

    @Override
    public int set_num_of_passengers() {
        return 400;
    }

    @Override
    public boolean has_gas() {
        return false;
    }

    @Override
    public void print() {
        System.out.println("Class Name: Plane");
    }
}
