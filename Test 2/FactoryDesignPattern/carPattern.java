package com.company.FactoryDesignPattern;

import org.omg.Messaging.SyncScopeHelper;

public class carPattern implements vehiclePattern {
    @Override
    public int set_num_of_wheels() {
        return 4;
    }

    @Override
    public int set_num_of_passengers() {
        return 5;
    }

    @Override
    public boolean has_gas() {
        return true;
    }

    @Override
    public void print() {
        System.out.println("Class Name: Car");

    }
}
