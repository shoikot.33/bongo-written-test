package com.company.FactoryDesignPattern;

public class vehicleFactory {
    public vehiclePattern getInstance(int num_of_wheels){
        if(num_of_wheels==4){
            return  new carPattern();
        }
        else
            return new planePattern();
    }
}
