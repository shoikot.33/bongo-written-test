package com.company.FactoryDesignPattern;

public interface vehiclePattern {
    int set_num_of_wheels();
    int set_num_of_passengers();
    boolean has_gas();
    void print();
}
